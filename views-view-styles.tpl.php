<?php
/**
 * @file
 * Displays the items of the FLIP Style.
 *
 * @ingroup views_templates
 *
 * Note that the FLIP Style NEEDS <?php print $row ?> to be wrapped by an
 * element, or it will hide all fields on all rows under the first field.
 *
 * Also, if you use field grouping and use the headers of the groups as the
 * FLIP Style headers, these NEED to be inside h3 tags exactly as below
 * (though you can add classes).
 */
?>
<div class="views-flip-style">
  <div class="container">
    <div class="scene3D">
      <div class="flip">
        <?php if (!empty($title)): ?>
          <div>
            <?php print $title; ?>
          </div>
        <?php endif; ?>
        <?php foreach ($rows as $id => $row): ?>
          <?php print $row ?>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</div>
