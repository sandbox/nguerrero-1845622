<?php
/**
 * @file
 * Provide an FLIP Style for Views. This file is autoloaded by views.
 */

/**
 * Implements hook_views_plugin().
 */
function views_styles_views_plugins() {
  return array(
    'style' => array(
      'views_styles' => array(
        'title' => t('Views Styles'),
        'theme' => 'views_view_styles',
        'help' => t('Blaublau'),
        'handler' => 'views_styles_style_plugin',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
