<?php
/**
 * @file
 * Provide an flip style plugin for views. This file is autoloaded by views.
 */

/**
 * Implements views_plugin_style().
 */
class views_styles_style_plugin extends views_plugin_style {

  /**
   * Set default options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['use-grouping-header'] = array('default' => 1);
    $options['include-style'] = array('default' => 1);
    return $options;
  }

  /**
   * Render the apparance in this style.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['grouping']['#prefix'] = '<div class="form-item">' . t('<strong>IMPORTANT:</strong> The <em>first field</em> in order of appearance <em>will</em> be the one used as the "header" or "trigger" of the accordion action.') . '</div>';
    // Available valid options for grouping.
    foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
      $options[] = $field;
    }
    $form['use-grouping-header'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use the group header as the Flip element'),
      '#default_value' => $this->options['use-grouping-header'],
      '#description' => t("If checked, the Group\'s header will be used to do the flip effect."),
      '#process' => array('views_process_dependency'),
      '#dependency' => array('edit-style-options-grouping' => $options),
    );
    $form['include-style'] = array(
      '#type' => 'checkbox',
      '#title' => t("Use the module's default styling"),
      '#default_value' => $this->options['include-style'],
      '#description' => t("If you disable this, the file in the module's directory <em>views-accordion.css</em> will not be loaded"),
    );
  }

  /**
   * Render the display in this style.
   */
  public function render() {
    $output = parent::render();
    $view_settings = array();
    // Default settings:.
    $view_settings['grouping'] = $this->options['grouping'] ? 1 : 0;
    $view_settings['usegroupheader'] = $view_settings['grouping'] ? $this->options['use-grouping-header'] : 0;
    if ($view_settings['usegroupheader'] == 1) {
      $view_settings['header'] = 'h3.' . $view_id;
    }
    if ($view_settings['usegroupheader'] == 0) {
      $i = 0;
      foreach ($this->view->field as $id => $value) {
        if (($i == 0) && ($value->options['exclude'] == 0)) {
          $view_settings['header'] = 'views-field-' . str_replace('_', '-', $id);
          $i++;
        }
      }
    }

    $view_id = 'views-css-flip-' . $this->view->name . '-' . $this->view->current_display;
    // We add the js settings:
    drupal_add_js(array('views_styles' => array($view_id => $view_settings)), 'setting');
    if ($this->options['include-style']) {
      // Add the css file:.
      drupal_add_css(drupal_get_path('module', 'views_styles') . '/views_styles_flip.css');
    }
    return $output;
  }
}
